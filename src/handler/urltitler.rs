extern crate regex;
use regex::Regex;

extern crate htmlstream;

use hyper::header::{Cookie, CookiePair, UserAgent, ContentType};
use hyper::mime::{Mime, TopLevel, SubLevel};
use hyper::client::Client;

use std::io::Read;

use handler::{MessageHandler, HandlerResult};
use message::IncomingMessage;

pub struct UrlTitler {
    regex: Regex
}

impl UrlTitler {
    pub fn new() -> UrlTitler {
        UrlTitler {
            regex: regex!(r"https?:[^\s]+")
        }
    }
}

impl MessageHandler for UrlTitler {
    fn name(&self) -> &str {
        "UrlTitler"
    }

    fn re(&self) -> &Regex {
        &self.regex
    }

    fn handle(&self, incoming: &IncomingMessage) -> HandlerResult {
        if let Some(url) =
            self.get_captures(incoming.get_contents())
            .and_then(|cap| cap.at(0)) {
                let client = Client::new();
                if let Ok(mut res) = client.get(url)
                    .header(UserAgent("Firefox".to_owned())) // fake UA to access Facebook
                    .header(Cookie(vec![CookiePair::new(
                        // cookie to access NYtimes articles
                        "NYT-S".to_owned(),
                        "0MCHCWA5RI93zDXrmvxADeHLKZwNYF3ivqdeFz9JchiAIUFL2BEX5FWcV.Ynx4rkFI".to_owned())]))
                    .send() {
                        match res.headers.get::<ContentType>() {
                            // Facebook returns text/html with no options
                            Some(&ContentType(Mime(TopLevel::Text, SubLevel::Html, _))) => {
                                let mut html = String::new();
                                try!(res.read_to_string(&mut html));
                                try!(incoming.reply(
                                    format!("TITLE: {}",
                                            htmlstream::tag_iter(&html)
                                            .skip_while(|&(ref _pos, ref tag)| tag.name != "title".to_owned())
                                            .skip(1).take(1).next().expect("No title")
                                            .1.html.trim())));
                            },
                            _ => { },
                        }
                    } else {
                        println!("Failed to get {}", url);
                    }
            }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use handler::UrlTitler;
    use message::{AdapterMsg, IncomingMessage};
    use handler::MessageHandler;
    use std::sync::mpsc::channel;

    #[test]
    #[allow(unused_variables)]
    fn test_create() {
        let handler = UrlTitler::new();
    }

    #[test]
    fn test_valid_urls() {
        let handler = UrlTitler::new();

        assert!(handler.can_handle("https://github.com"));
        assert!(handler.can_handle("http://github.com"));
        assert!(!handler.can_handle("ftp://example.com"));
    }

    #[test]
    fn test_response() {
        let handler = UrlTitler::new();
        let msg = "words and words http://chatbot.rs/chatbot/";
        let (tx, rx) = channel();
        let inc = IncomingMessage::new(handler.name().to_owned(),
                                       None, None, None, msg.to_owned(), tx);

        handler.handle(&inc).unwrap();

        match rx.recv().unwrap() {
            AdapterMsg::Outgoing(ref message) => {
                assert_eq!(message.as_ref(),
                           "TITLE: chatbot - Rust");
            },
            _ => panic!("Did not receive message from handler")
        };
    }
}
